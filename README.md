<h1 align="center">Kiizuha's Genshin Stats</h1>
<h3 align="center">
  🕒 Updated at <u>Oct 11, 2023 19:17 +0700</u> (🤖automated)
</h3>
<br />

<h2>Daily Rewards</h2>
<table>
  <tr>
    <td>Total Rewards Claimed</td>
    <td>11</td>
  </tr>
  <tr>
    <td>Last Claimed Reward</td>
    <td>20 x Primogem</td>
  </tr>
</table>

<h2>Stats</h2>
<table>
  <tr>
    <td>Achievements</td>
    <td>515</td>
  </tr>
  <tr>
    <td>Days Active</td>
    <td>409</td>
  </tr>
  <tr>
    <td>Characters</td>
    <td>42</td>
  </tr>
  <tr>
    <td>Waypoints Unlocked</td>
    <td>249</td>
  </tr>
  <tr>
    <td>Anemoculi</td>
    <td>65</td>
  </tr>
  <tr>
    <td>Geoculi</td>
    <td>131</td>
  </tr>
  <tr>
    <td>dendroculi</td>
    <td>70</td>
  </tr>
  <tr>
    <td>Electroculi</td>
    <td>181</td>
  </tr>
  <tr>
    <td>hydroculi</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Common Chests Opened</td>
    <td>1360</td>
  </tr>
  <tr>
    <td>Exquisite Chests Opened</td>
    <td>1086</td>
  </tr>
  <tr>
    <td>Precious Chests Opened</td>
    <td>308</td>
  </tr>
  <tr>
    <td>Luxurious Chests Opened</td>
    <td>126</td>
  </tr>
  <tr>
    <td>Remarkable Chests Opened</td>
    <td>55</td>
  </tr>
  <tr>
    <td>Domains Unlocked</td>
    <td>39</td>
  </tr>
</table>

<h2>Exploration</h2>
<table>
  <tr>
    <th>Sumeru</th>
    <th>The Chasm: Underground Mines</th>
    <th>The Chasm</th>
    <th>Enkanomiya</th>
    <th>Inazuma</th>
    <th>Dragonspine</th>
    <th>Liyue</th>
    <th>Mondstadt</th>
    <th>Fontaine</th>
  </tr>
  <tr>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Xumi.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_ChasmsMaw.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_ChasmsMaw.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Enkanomiya.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Daoqi.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Dragonspine.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Liyue.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Mengde.png"
          width="180"
        />
      </p>
    </td>
    <td>
      <p align="center">
        <img
          src="https://upload-os-bbs.mihoyo.com/game_record/genshin/city_icon/UI_ChapterIcon_Fengdan.png"
          width="180"
        />
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/20/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Reputation</td>
          <td>1</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/100/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Offering</td>
          <td>10</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/100/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Offering</td>
          <td>10</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/94/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Offering</td>
          <td>0</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/100/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Reputation</td>
          <td>10</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/100/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Offering</td>
          <td>12</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/82/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Reputation</td>
          <td>6</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/98/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Reputation</td>
          <td>6</td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Explored</td>
          <td>
            <img src="https://progress-bar.dev/0/" width="80" />
          </td>
        </tr>
        <tr>
          <td>Reputation</td>
          <td>1</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<h2>Teapot</h2>
<table>
  <tr>
    <td>Level</td>
    <td>6</td>
  </tr>
  <tr>
    <td>Comfort</td>
    <td>Exquisite (8130)</td>
  </tr>
  <tr>
    <td>Items</td>
    <td>369</td>
  </tr>
  <tr>
    <td>Visitors</td>
    <td>8</td>
  </tr>
</table>
<h2>Diary</h2>
<table>
  <tr>
    <td>Primogems earned</td>
    <td>1428 (rate -50)</td>
  </tr>
  <tr>
    <td>Mora earned</td>
    <td>230070 (rate -80)</td>
  </tr>
  <tr>
    <td>Categories</td>
    <td>
      Mail: 1280 (90%)<br />Events: 60 (4%)<br />Daily Activity: 60 (4%)<br />Adventure:
      22 (1%)<br />Spiral Abyss: 0 (0%)<br />Quests: 0 (0%)<br />Other: 6
      (1%)<br />
    </td>
  </tr>
</table>

<h2>Characters</h2>
<details open>
  <summary><b>Ganyu</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Ganyu</h3></th>
      <th><h3 align="center">Amos' Bow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Ganyu.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/6c24b865bc36e2538d1dd9d76e3abf92.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>1 x Gladiator's Finale<br />4 x Wanderer's Troupe<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Amos' Bow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Klee</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Klee</h3></th>
      <th><h3 align="center">Dodoco Tales</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Klee.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/60e0660f93fdf50bf211d7121aeea29f.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              4 x Crimson Witch of Flames<br />1 x Gladiator's Finale<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dodoco Tales</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>5</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Kamisato Ayaka</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Kamisato Ayaka</h3></th>
      <th><h3 align="center">Amenoma Kageuchi</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Ayaka.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b3a74345a17eeae9048c2b030e14c6d8.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>1 x Wanderer's Troupe<br />4 x Blizzard Strayer<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Amenoma Kageuchi</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>2</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Kaedehara Kazuha</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Kaedehara Kazuha</h3></th>
      <th><h3 align="center">Iron Sting</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Kazuha.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/2c1c6d8ddebc7261c3f30cb72763bd45.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Anemo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Viridescent Venerer<br />1 x Noblesse Oblige<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Iron Sting</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>2</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Raiden Shogun</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Raiden Shogun</h3></th>
      <th><h3 align="center">"The Catch"</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Shougun.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/11887c90fc4423c04805966f479bbf46.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              1 x Gladiator's Finale<br />4 x Emblem of Severed Fate<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>"The Catch"</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>5</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Xiangling</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Xiangling</h3></th>
      <th><h3 align="center">Dragon's Bane</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Xiangling.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/6cff7996816dee8175a0f44d58a20165.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Crimson Witch of Flames<br />1 x Blizzard Strayer<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dragon's Bane</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Xingqiu</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Xingqiu</h3></th>
      <th><h3 align="center">Sacrificial Sword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Xingqiu.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/62eeb6d0df41298df63847db918c4fda.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Hydro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>2 x Heart of Depth<br />3 x Noblesse Oblige<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Sacrificial Sword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Noelle</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Noelle</h3></th>
      <th><h3 align="center">Whiteblind</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Noel.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/93f24e5abd545a90b8ee7d799f72db41.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Geo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              1 x Wanderer's Troupe<br />2 x Crimson Witch of Flames<br />2 x
              Defender's Will<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Whiteblind</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>3</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Nahida</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Nahida</h3></th>
      <th><h3 align="center">Mappa Mare</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Nahida.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/61ecbfa9e44de683dc5e3527445f2104.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Dendro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>90</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>3 x Deepwood Memories<br />2 x Gilded Dreams<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Mappa Mare</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>60</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>5</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Sucrose</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Sucrose</h3></th>
      <th><h3 align="center">Mappa Mare</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Sucrose.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/61ecbfa9e44de683dc5e3527445f2104.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Anemo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Instructor<br />1 x Gladiator's Finale<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Mappa Mare</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Bennett</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Bennett</h3></th>
      <th><h3 align="center">Aquila Favonia</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Bennett.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/eeda94a7cad5b698d6af1c1465b1e12d.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>9</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Noblesse Oblige<br />1 x Emblem of Severed Fate<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Aquila Favonia</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Diluc</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Diluc</h3></th>
      <th><h3 align="center">Luxurious Sea-Lord</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Diluc.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/c6622cf416c178e2ed51bef8adc39d6c.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              3 x Crimson Witch of Flames<br />2 x Gladiator's Finale<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Luxurious Sea-Lord</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>60</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Shenhe</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Shenhe</h3></th>
      <th><h3 align="center">Dragon's Bane</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Shenhe.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/6cff7996816dee8175a0f44d58a20165.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>80</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>2 x Gladiator's Finale<br />3 x Blizzard Strayer<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dragon's Bane</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Diona</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Diona</h3></th>
      <th><h3 align="center">Favonius Warbow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Diona.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/2bc2401bd90c3498393a964df6f2dac0.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              1 x Heart of Depth<br />2 x Wanderer's Troupe<br />1 x Shimenawa's
              Reminiscence<br />1 x Emblem of Severed Fate<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Favonius Warbow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Lisa</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Lisa</h3></th>
      <th><h3 align="center">Magic Guide</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Lisa.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/dfcad9ae9de73433ac6ea64ead2b1408.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>7</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>2 x Instructor<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Magic Guide</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Qiqi</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Qiqi</h3></th>
      <th><h3 align="center">Sacrificial Sword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Qiqi.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/62eeb6d0df41298df63847db918c4fda.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Maiden Beloved<br />1 x Gladiator's Finale<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Sacrificial Sword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>60</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Kaeya</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Kaeya</h3></th>
      <th><h3 align="center">Sacrificial Sword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Kaeya.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/62eeb6d0df41298df63847db918c4fda.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Instructor<br />1 x Prayers to Springtime<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Sacrificial Sword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Yae Miko</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Yae Miko</h3></th>
      <th><h3 align="center">Oathsworn Eye</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Yae.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/1d775cb4cde940f8c66504d879134fc1.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Gilded Dreams<br />1 x Heart of Depth<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Oathsworn Eye</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>60</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>5</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Traveler</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Traveler</h3></th>
      <th><h3 align="center">Prototype Rancour</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_PlayerBoy.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/6cd63c7ee556624c7eaa350947e2d956.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Geo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>70</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>3 x Gladiator's Finale<br />2 x Berserker<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Prototype Rancour</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Chongyun</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Chongyun</h3></th>
      <th><h3 align="center">Sacrificial Greatsword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Chongyun.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/96c09c61c7470087de39f96230e83bb0.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>60</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>5 x Instructor<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Sacrificial Greatsword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Mona</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Mona</h3></th>
      <th><h3 align="center">Favonius Codex</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Mona.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/6bde6d8b6ef7b7a0ae9e00082de40809.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Hydro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              3 x Wanderer's Troupe<br />1 x Heart of Depth<br />1 x Gladiator's
              Finale<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>Pact of Stars and Moon</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Favonius Codex</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Beidou</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Beidou</h3></th>
      <th><h3 align="center">Debate Club</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Beidou.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/276e1af241844682cba621bad65d700e.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              1 x Lucky Dog<br />3 x Martial Artist<br />1 x Prayers for
              Wisdom<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Debate Club</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Barbara</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Barbara</h3></th>
      <th><h3 align="center">Thrilling Tales of Dragon Slayers</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Barbara.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/bee929c1d8e0f0f68cd8684e5c10d229.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Hydro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>4 x Maiden Beloved<br />1 x Gladiator's Finale<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>Summertime Sparkle</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Thrilling Tales of Dragon Slayers</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Amber</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Amber</h3></th>
      <th><h3 align="center">Favonius Warbow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Ambor.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/2bc2401bd90c3498393a964df6f2dac0.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>5 x Berserker<br /></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>100% Outrider</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Favonius Warbow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>30</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Fischl</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Fischl</h3></th>
      <th><h3 align="center">Favonius Warbow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Fischl.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/2bc2401bd90c3498393a964df6f2dac0.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              2 x Traveling Doctor<br />2 x Adventurer<br />1 x Berserker<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>Ein Immernachtstraum</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Favonius Warbow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>30</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Collei</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Collei</h3></th>
      <th><h3 align="center">Hunter's Bow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Collei.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/dd940ee465fc6ccf2f11f97951632a45.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Dendro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td>
              2 x Wanderer's Troupe<br />1 x Gladiator's Finale<br />2 x
              Deepwood Memories<br />
            </td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Hunter's Bow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Yun Jin</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Yun Jin</h3></th>
      <th><h3 align="center">Beginner's Protector</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Yunjin.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b2fd56c2685792654b83c15498cc8681.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Geo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Beginner's Protector</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Ningguang</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Ningguang</h3></th>
      <th><h3 align="center">Apprentice's Notes</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Ningguang.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/4fc472e727daebbc10133272195b31c2.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Geo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>Orchid's Evening Gown</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Apprentice's Notes</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Aloy</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Aloy</h3></th>
      <th><h3 align="center">Hunter's Bow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Aloy.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/dd940ee465fc6ccf2f11f97951632a45.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Hunter's Bow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Xinyan</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Xinyan</h3></th>
      <th><h3 align="center">Waster Greatsword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Xinyan.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b6d2be6fba2068ea945c08b2319564d6.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Waster Greatsword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Rosaria</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Rosaria</h3></th>
      <th><h3 align="center">Beginner's Protector</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Rosaria.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b2fd56c2685792654b83c15498cc8681.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td>To the Church's Free Spirit</td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Beginner's Protector</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Yanfei</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Yanfei</h3></th>
      <th><h3 align="center">Apprentice's Notes</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Feiyan.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/4fc472e727daebbc10133272195b31c2.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Apprentice's Notes</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Sayu</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Sayu</h3></th>
      <th><h3 align="center">Waster Greatsword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Sayu.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b6d2be6fba2068ea945c08b2319564d6.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Anemo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Waster Greatsword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Kujou Sara</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Kujou Sara</h3></th>
      <th><h3 align="center">Hunter's Bow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Sara.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/dd940ee465fc6ccf2f11f97951632a45.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Hunter's Bow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Shikanoin Heizou</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Shikanoin Heizou</h3></th>
      <th><h3 align="center">Apprentice's Notes</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Heizo.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/4fc472e727daebbc10133272195b31c2.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Anemo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Apprentice's Notes</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Keqing</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Keqing</h3></th>
      <th><h3 align="center">Dull Blade</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Keqing.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b69ad8c7d2095dfaa1fdfc2cdc872a07.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>5</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dull Blade</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Razor</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Razor</h3></th>
      <th><h3 align="center">Waster Greatsword</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Razor.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b6d2be6fba2068ea945c08b2319564d6.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Electro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>2</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Waster Greatsword</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Thoma</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Thoma</h3></th>
      <th><h3 align="center">Beginner's Protector</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Tohma.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b2fd56c2685792654b83c15498cc8681.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Pyro</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Beginner's Protector</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Gorou</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Gorou</h3></th>
      <th><h3 align="center">Hunter's Bow</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Gorou.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/dd940ee465fc6ccf2f11f97951632a45.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Geo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>3</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Hunter's Bow</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Layla</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Layla</h3></th>
      <th><h3 align="center">Dull Blade</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Layla.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b69ad8c7d2095dfaa1fdfc2cdc872a07.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dull Blade</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Mika</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Mika</h3></th>
      <th><h3 align="center">Beginner's Protector</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Mika.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b2fd56c2685792654b83c15498cc8681.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Cryo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Beginner's Protector</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
<details>
  <summary><b>Lynette</b></summary>
  <br />
  <table>
    <tr>
      <th><h3 align="center">Lynette</h3></th>
      <th><h3 align="center">Dull Blade</h3></th>
    </tr>
    <tr>
      <td>
        <p align="center">
          <img
            src="https://upload-os-bbs.mihoyo.com/game_record/genshin/character_icon/UI_AvatarIcon_Linette.png"
          />
        </p>
      </td>
      <td>
        <p align="center">
          <img
            src="https://act.hoyoverse.com/hk4e/e20200928calculate/item_icon_u38a6e/b69ad8c7d2095dfaa1fdfc2cdc872a07.png"
          />
        </p>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td>Rarity</td>
            <td>4</td>
          </tr>
          <tr>
            <td>Element</td>
            <td>Anemo</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Friendship</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Constellation</td>
            <td>0</td>
          </tr>
          <tr>
            <td>Artifacts</td>
            <td></td>
          </tr>
          <tr>
            <td>Outfits</td>
            <td></td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Name</td>
            <td>Dull Blade</td>
          </tr>
          <tr>
            <td>Rarity</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Level</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Refinement</td>
            <td>1</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</details>
